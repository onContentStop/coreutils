use std::str::FromStr;

pub struct PartialChar {
    bytes: [u8; 6],
    num_bytes: u8,
    pub num_bytes_left: u8,
}

impl PartialChar {
    pub fn new() -> Self {
        Self {
            bytes: [0; 6],
            num_bytes: 0,
            num_bytes_left: 0,
        }
    }

    pub fn start_char(&mut self, byte: u8) -> Result<(), String> {
        // start at third bit
        let mut mask = 0x20;
        self.num_bytes_left = 1;
        while byte & mask != 0 {
            if mask == 0x02 {
                // too many set bits to be valid
                return Err(format!("Invalid UTF-8 start byte: 0x{:2x}", byte));
            }
            self.num_bytes_left += 1;
            mask >>= 1;
        }
        self.num_bytes = 1;
        self.bytes[0] = byte;
        Ok(())
    }

    pub fn continue_char(&mut self, byte: u8) -> Result<(), String> {
        // expected: first two bits are 0b10
        if byte & 0xc0 != 0x80 {
            return Err(format!("Invalid UTF-8 continuation byte: 0x{:2x}", byte));
        }
        self.num_bytes_left -= 1;
        self.bytes[self.num_bytes as usize] = byte;
        self.num_bytes += 1;
        Ok(())
    }

    pub fn finish_char(&self) -> Result<char, String> {
        if self.num_bytes_left != 0 || self.num_bytes == 0 {
            return Err(String::from("does not contain a finished UTF-8 char"));
        }

        // unwrapping here because UTF-8 was already validated
        let c = std::str::from_utf8(&self.bytes[..self.num_bytes as usize]).unwrap();
        Ok(char::from_str(c).unwrap())
    }
}
