#[derive(Debug)]
pub struct Configuration {
    pub show_nonprinting: bool,
    pub show_tabs: bool,
    pub squeeze_blanks: bool,
    pub number_lines: bool,
    pub show_line_ends: bool,
    pub number_nonblanks: bool,
}

impl Default for Configuration {
    fn default() -> Self {
        Configuration {
            show_nonprinting: false,
            show_tabs: false,
            show_line_ends: false,
            squeeze_blanks: false,
            number_lines: false,
            number_nonblanks: false,
        }
    }
}

impl Configuration {
    pub fn show_all(&mut self) {
        self.show_nonprinting = true;
        self.show_tabs = true;
        self.show_line_ends = true;
    }

    pub fn show_nonprinting_and_tabs(&mut self) {
        self.show_nonprinting = true;
        self.show_tabs = true;
    }

    pub fn show_nonprinting_and_line_ends(&mut self) {
        self.show_nonprinting = true;
        self.show_line_ends = true;
    }

    pub fn number_lines(&mut self) {
        if !self.number_nonblanks {
            self.number_lines = true;
        }
    }
}
