use common::Arg;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), ()> {
    let raw_args: Vec<String> = env::args().collect();
    let args = common::parse_args(&raw_args);
    let mut files: Vec<String> = vec![];
    for arg in &args[1..] {
        if let Arg::Positional(text) = arg {
            files.push(text.to_owned());
        }
    }

    for file in files {
        println!("{}: ", file);
        let handle = File::open(&file).map_err(|e| {
            eprintln!("ERROR: Opening {}: {}", file, e);
            ()
        })?;
        let mut reader = BufReader::new(handle);
        const BYTES_PER_LINE: u64 = 8;
        let mut byte_index = 0u64;
        let mut address = 0u64;
        print!("{:8x}: ", address);
        loop {
            let buf = reader.fill_buf().map_err(|e| {
                eprintln!("ERROR: Reading from {}: {}", file, e);
                ()
            })?;
            let consume = buf.len();
            if consume == 0 {
                break;
            }
            for byte in buf {
                print!("{:2x} ", byte);
                byte_index += 1;
                if byte_index == BYTES_PER_LINE {
                    byte_index = 0;
                    address += BYTES_PER_LINE * 8;
                    println!();
                    print!("{:8x}: ", address);
                }
            }
            reader.consume(consume);
        }
    }
    println!();
    Ok(())
}
